# Sécurité informatique

## Sommaire

* Savoir Attaquer
    * La méthodologie pentest
    * Phase 1: Récupération passive
    * Phase 2: Récupération active
    * Phase 3: Gain de privilèges
    * Phase 4: Maintien de l'accès
    * Phase 5: Nettoyage des traces
* Savoir se défendre
    * Détecter et/ou Bloquer une attaque
    * Les bonnes pratiques
* Les TPs
    * Récupération d'informations passives
    * Prise en main de NMAP/Zenmap
    * Se mettre en Man in the middle avec Ettercap
    * Mise en place de DNS poisoning
    * Utilisation du Social Engineering Toolkit (SET)
    * Bruteforce avec patator
    * Bruteforce avec Hydra
    * Déni de service avec hping
    * Casser les mots de passe windows avec OphCrack
    * Créer un utilisateur administrateur sous windows XP
    * Detecter des vulnérabilités avec Nessus ou Nikto
    * Detecter et exploiter avec Metasploit
    * Sortir des informations de la RAM avec volatility
    * Cacher des fichiers dans du NTFS
    * Mettre en place SNORT et snorby
    * Mettre en place du port knocking
    * Se prémunir du DHCP spoofing
    * Se protéger des attaques SMURF
    * Se protéger de l'ARP spoofing
  
## Savoir Attaquer

Pour pouvoir se protéger d'une attaque, il est important de savoir comment l'assaillant à effectuer son attaque.
Il est donc important de comprendre les méthodes et les outils qu'il utilise. Dans la suite de ce cours nous verrons
donc la méthodologie habituellement utilisée par les professionnels de la sécurité informatique pour effectuer un test
d'intrusion dans une société.

### La méthodologie pentest

Un pentest est un test intrusion effectué par un professionnel dans le but de tester la sécurité du système d'information
d'une société. Cet audit de sécurité permet à la société de connaitre les vulnérabilités présentes sur son réseau pour
ainsi pouvoir les corriger avant qu'elles soient exploitées par un utilisateur malveillant.

Pour que les résultats soient les plus réalistes possible, les pentesters ont mis au point une méthodologie type,
permettant de tester la sécurité d'un système de manière procédurale et organisée. Grace à cette méthodologie, il est
plus rare de passer à côté d'un élément important qui aurait pu être oublié sans ordonnancement des attaques.

Cette méthodologie a été mise au point en étudiant les habitudes et les méthodologies utilisées par des hackers contre
des sociétés. De cette observation, les pentesters ont pu diviser une attaque en cinq phases:

* Récupération d'informations passive
* Récupération d'informations active
* Gain de privilèges
* Maintien de l'accès
* Nettoyage des traces

Nous verrons dans ce cours, les 5 étapes d'un test d'intrusion.

### Phase 1: Récupération passive

#### Présentation

La première phase d'une attaque ou d'un audit sur un système d'information consiste à récupérer le plus d'information
possible de manière discrète. Il s'agit souvent d'information sur la société et ses employés. 

Le terme «passive» est utilisé car il s'agit de récupérer des informations sans effectué d'action sur le système 
d'information.

Cette phase permet de préparer l'attaque en connaissant par exemple la hierarchie dans l'entreprise, le nom de plusieurs
employés, leurs adresses mails et parfois même leur numéro de téléphone. Toutes ses informations doivent être analyser
pour pouvoir s'en servir dans la suite de l'audit.

#### Les outils

La récupération d'information passive peut se faire de très nombreuse manière. Il est par exemple possible de récupérer
des informations sur une société directement à la Chambre des commerces et de l'industrie (CCI) ou bien sur des sites tels
que societe.com. Les informations sur un employé peuvent être trouvées dans les pages jaunes ou sur Google et Facebook.
Le nombre de sources d'informations est devenu important avec les années. Aujourd'hui, il existe une discipline appelée
OSINT, pour Open Source INTelligence. Il s'agit de récupérer des informations dans des sources librement consultable, et
de les recouper pour obtenir des données plus complête. 

##### Google dorks

###### Kesako ?

Si Google est connu, c'est bien entendu grace à son moteur de recherche qui domine
le marché depuis des années devant Yahoo et Bing. Pourtant  la majorité des utilisateurs
ne connaissent pas l'étendu des fonctionnalités proposées par ce dernier. Les google
dorks utilisent ses fonctionnalités, et sont bien pratique lorsque l'on veut optimiser
une recherche.

En effet, le barre de recherche google n'accepte pas que des mots clés pour effectuer
une recherche, il est aussi possible de faire appel à des fonctions pour affiner ses critères.

C'est ces fonctions que nous allons utiliser lors de ce workshop et qui donne lieu à des Google dorks.

###### Opérateurs de recherche

Les fonctions présentent dans le tableau suivant s'utilisent directement dans la barre de recherche
et respecte la même syntaxe "nom_fonction:argument".

Voici une liste des fonctions importantes

| Fonction   | Description                                                               |
|:----------:|---------------------------------------------------------------------------|
| intitle    | Recherche des pages contenant des mots clés dans le titre                 |
| intext     | Recherche des pages contenant des mots clés dans le corps de la page      |
| inurl      | Recherche un pattern dans l'url de la page                                |
| allintitle | Recherche des pages contenant tout les mots clés dans le titre            |
| allintext  | Recherche des pages contenant tout les mots clés dans le corps de la page |
| allinurl   | Vous aurez compris le principe non ?                                      |
| site       | Recherche toutes les pages indexées pour un domaine donné                 |
| filetype   | Recherche une page comportant l'extension spécifiée                       |
| link       | Recherche les sites ayant un lien qui pointe vers l'adresse spécifiée     |
| related    | Recherche les sites ayant des points communs avec l'adresse spécifiée     |
| cache      | Recherche la version mise en cache par google de l'adresse spécifiée      |
| info       | Permet de récupérer les informations concernant une URL (cache, related)  |

En plus des fonctions, Google propose des opérateurs plus simple permettant de filter une recherche

| Opérateur | Description                                             | Exemple                      |
|:---------:|---------------------------------------------------------|------------------------------|
| -         | Exclue le terme préceder par cette operateur            | football -ballon             |
| ~         | Permet d'effectuer une recherche par synonyme           | allintitle:~private document |
| OR        | Recherche un mot ou un autre                            | rond OR carré                |
| ..        | Filtre les résultats contenu dans un interval de nombre | 10..100                      |
| *         | Permet d'agir comme mot joker dans un phrase            | Google * ma vie              |

Enfin il est possible de rechercher les termes exacts en entourant la phrase par des guillemets.

##### Facebook graph search

##### Les autres réseaux sociaux

##### societe.com

##### Shodan

##### TheHarvester

##### Maltego

### Phase 2: Récupération active

La seconde étape d'un pentest consiste à récupérer des informations de manière active. C'est à dire que cette fois, le pentester
récupère des informations directement sur le système. Pour ce faire, il se base sur les informations récupérées précédemment pour
continuer de manière discrète. Le but de cette phase est souvent de d'avoir une vision plus précise du système informatique en 
détectant les machines présentent sur le réseau ainsi que les services qu'elles proposent. Cette phase étant active, il est plus
facile des pour les DSI de détecter l'intrusion.

#### NMAP

##### Présentation

NMAP est un outil développé à l'origine pour découvrir les hôtes présent sur un
réseau et pour voir quels ports sont ouverts sur ceux-ci afin d'y voir de potentiels
vulnérabilités.

Depuis le logiciel s'est doté de plugin lui permettant de faire bien plus et notamment
d'attaquer des cibles automatiquement si des vulnérabilités semblent être présentes.
Nous allons tenter dans ce workshop d'apprendre à utiliser NMAP et quelques-uns de
ses plugins.

##### Premiers pas

L'utilisation de NMAP se fait en ligne commande, nous allons donc commencer par
apprendre une commande très importantes de l'outil:

    nmap -h

Cette commande permet d'afficher l'aide décrivant les options disponibles pour le
logiciel.

Lors d'un audit l'utilisation de NMAP se fait souvent lors de la phase de prise
d'information du système d'information. Nous allons donc voir comment effectuer
un scan et pourquoi il existe autant de types.

##### Découvertes d'hôtes

###### ARP scan

** Présentation **

Envoi des requête ARP et attend la réponse.

** Utilisation **

    nmap -sn adresse_reseau_cible

###### Idle scan

** Presentation **

Utilisation IPID, 30 ports, dicotomie
Passe au travers du firewall

** Utilisation **

    nmap -sI adresse_zombie adresse_cible

###### List scan

** Description **

Résolution DNS

** Utilisation **

    nmap -sL adresse_reseau_cible
    nmap -sL -n adresse_reseau_cible

##### Scan de ports

###### TCP SYN

** Description **

Une requête TCP avec le flag SYN est envoyée à la machine cible, si
la machine distante répond par un ACK alors le port est ouvert, si elle répond
par un RST, alors le port est fermé.

Le scan execute un handshake dit 'half open' car après avoir reçu une réponse du
serveur, l'attaquant ferme la connexion sans la valider. Le handshake est donc incomplet.

** Utilisation **

    nmap -sS adresse_cible

###### TCP connect()

** Description **

A l'instar du scan TCP SYN, le scan TCP connect() utilise un handshake TCP pour savoir si
le port est ouvert. La différence avec le scan précédent est que la connexion est établi à
la fin du scan. Le handshake n'est plus 'half open'

** Utilisation **

    nmap -sT adresse_cible

###### FIN scan

** Description **

Ce scan est dit 'discret' car aucune connexion n'est établie avec la cible. Aucuns paquets de 
synchronisation n'est envoyé, il est donc rare que le scan puisse être logué.

L'attaquant envoi un paquet avec le drapeau FIN levé. Si le port est fermé, alors la cible répondra
par un paquet TCP RST (qu'il enverrai à n'importe quel requête). Néanmoins si le port est ouvert,
le serveur verra qu'il s'agit d'une demande pour arrêter une connexion inexistante. De ce fait il
ne répondra pas. L'absence de réponse peut aussi être causé par la présence d'un firewall filtrant 
le port

** Utilisation **

    nmap -sF adresse_cible

###### Null scan

** Description **

Il s'agit d'un scan très similaire au scan FIN à contrario que tout les flags TCP sont descendu.
L'absence de réponse peut aussi être causé par la présence d'un firewall filtrant le port.

** Utilisation

    nmap -sN adresse_cible

###### Xmas scan

** Description **

Il s'agit d'un scan très similaire au scan FIN à contrario que tout les flags TCP sont levés.
L'absence de réponse peut aussi être causé par la présence d'un firewall filtrant le port.

** Utilisation **

    nmap -sX adresse_cible

###### UDP scan

** Description **

Contrairement aux autres type de scan, celui-ci permet de détecter les ports UDP et non pas
les ports TCP. Dans ce scan, nous envoyons une requête UDP standard, si le serveur répond par
un paquet ICMP 'Destination Unreachable' alors le port est fermé, s'il n'y a pas de réponse
alors le port est ouvert ou filtré. Enfin si la cible répond avec un paquet UDP le port est ouvert.

** Utilisation **

    nmap -sU adresse_cible

###### ACK scan

** Description **

Ce type de scan permet de savoir si un port est filtré et si l'hôte est UP. 
Un paquet TCP avec le flag ACK est envoyé, si le port n'est pas filtré et 
que l'hôte est UP, un paquet TCP RST sera renvoyé. Dans le cas d'un réponse ICMP type
'Host unreachable' ou en l'absence de réponse, soit le port est filtré, soit un firewall 
est présent.
En cas de réponse RST, cela ne veut pas dire que le port est ouvert mais seulement qu'il
n'est pas filtré et que l'hôte répond.

** Utilisation **

    nmap -sA adresse_cible

###### Windows scan

** Description **

Le scan envoi un paquet TCP ACK, l'état du port est calculé en fonction de la taille de
l'entête window du paquet TCP RST.
Certaines stack TCP veulent qu'un port fermé est une window de 0 octet, alors qu'un port ouvert
doit avoir une window différente de 0 octet.

Ce scan est de moins en moins efficace car les stacks TCP n'ont plus ce genre de réaction.

** Utilisation **

    nmap -sW adresse_cible

##### Récupération d'informations

###### Version scan

** Description **

Après avoir lancé un simple scan de port, le version scan va tenter de récupérer la bannière
du service pour en connaitre la version. Pour celà, il va établir une connexion, récupérer la
bannière puis arrêter la connexion.

** Utilisation **

Ce scan peut être combiné avec n'importe quel scan de port, si aucun n'est défini alors il fera
un scan TCP connect()

    nmap -sV adresse_cible
    nmap -sS -sV adresse_cible
    nmap -sSV adresse_cible


### Phase 3: Gain de privilèges

** Description **

La phase de gain de privilège consiste à profiter de l'accès que l'on a obtenu durant la précédente étape pour obtenir des droits
supérieurs. Pour ce faire, l'attaquant utilise souvent une faille applicative afin d'obtenir des droits Administrateur (ou root).

### Phase 4: Maintien de l'accès

### Phase 5: Nettoyage des traces

## Savoir se défendre

### Détecter et/ou bloquer les attaques

### Port Knocking

Le port knocking est un système permettant de débloquer les port d'une machine en fonction d'une suite de «knock» reçue sur certains ports.
De ce fait, il est par exemple possible d'ouvrir un port ssh qu'après avoir reçu un knock sur le port 3000 et 8000, voir même 
de demander un knock de type TCP et l'autre de type UDP. Un knock est représenter par une trame TCP ou UDP visant un port 
particulier du système.

### Les bonnes pratiques

## Les TPs

### Récupération d'informations passives
### Prise en main de NMAP/Zenmap
### Se mettre en Man in the middle avec Ettercap
### Mise en place de DNS poisoning
### Utilisation du Social Engineering Toolkit (SET)
### Bruteforce avec patator
### Bruteforce avec Hydra
### Déni de service avec hping
### Casser les mots de passe windows avec OphCrack
### Créer un utilisateur administrateur sous windows XP
### Faire de même sous Windows Server 2012

Sous Windows Server 2012, la manipulation est très proche de la précédente:

* Ouvrir un invité de commande.
* Taper dans l'invité de commande, la commande **regedit**.
* Créer une clé nommée **sethc.exe** dans **HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options**
* Créer une valeur REG_SZ appelée *Debugger*.
* Indiquer **C:\Windows\System32\cmd.exe** en guise de valeur à *Debugger*.
* Presser 5 fois SHIFT.

### Detecter des vulnérabilités avec Nessus ou Nikto
### Detecter et exploiter avec Metasploit
### Sortir des informations de la RAM avec volatility
### Cacher des fichiers dans du NTFS

Pour cacher un fichier dans un système windows il suffit de s'asurer que le système de fichier utilisé est bien NTFS.
En effet, comme nous l'avons vu dans le cours, NTFS accepte un flux de donnée "alternatif". Nous allons donc voir comment faire ça
facilement.

On crée un fichier 'test.txt' contenant 'test'
    
    echo test > test.txt

Puis on créer un fichier 'hidden.txt' dans le flux alternatif
    
    echo test2 > test.txt:hidden.txt

On vérifie que hidden.txt n'apparait pas dans le dossier puis on l'affiche
    
    dir
    more < test.txt:hidden.txt

Pour supprimer le fichier rien de plus simple. Nous allons tout simplement lui donner une valeur nulle:

    TYPE NUL > test.txt:hidden.txt

### Mettre en place SNORT et snorby

#### Instalation de Mysql

Le système que nous allons installer néscessite la présence d'un serveur mysql.
Nous allons donc commencer par l'installation de celui-ci. Rien de plus simple
avec la commande:

  apt-get install mysql-server

#### Instalation de Snorby:

##### Prérequis

Nous allons installer Snorby à partir de son dépot GIT (aucun paquet n'étant dispo
pour Debian). Pour que l'installation se passe dans de bonne condition nous allons
installer les dépendences nécessaire à la construction du projet.

    apt-get install ruby1.9.3 git imagemagick wkhtmltopdf bundler
    # For nokogiri
    apt-get install libxml2-dev libxslt1-dev
    # For do_mysql
    apt-get install libmysqlclient-dev
    # For passenger
    apt-get install apache2 libcurl4-openssl-dev libssl-dev apache2-threaded-dev libapr1-dev libaprutil1-dev
    # For barnyard2 mysql-client
    apt-get install libpcap-dev 

Maintenant que c'est fait, nous allons télécharger et installer Snorby.

    cd /var/www
    git clone http://github.com/Snorby/snorby.git
    cd snorby
    bundle install

##### Configuration

Des fichiers d'exemple de configuration sont disponible dans le dossier 'config'
de Snorby, nous allons donc nous baser dessus pour configurer notre système.

    cd /var/www/snorby/config
    cp database.yml.example database.yml

Modifiez le fichier database.yml en entrant les identifiants permettant de se
logger sur la base de données. Pour le moment nous allons utiliser les identifiants
de l'utilisateur 'root' mais nous modifierons cela par la suite.

    cp snorby_config.yml.example snorby_config.yml

Il faut ensuite modifier le fichier snorby_config.yml pour entrer le chemin vers
le programme 'wkhtmltopdf' (un simple whereis wkhtmltopdf permet de le connaitre).

    bundle exec rake snorby:setup

Après cette commande la base de données devrait être correctement créée. Nous
allons donc maintenant créer un utilisateur avec des droits restreint à la
base de données utilisée par snorby.

    mysql -uroot -p
    
    create user 'snorby'@'localhost' IDENTIFIED BY 'password';
    grant all privileges on snorby.* to 'snorby'@'localhost' with grant option;
    flush privileges;

Après cette operation, nous allons à nouveau modifier le fichier database.yml
pour entrer les identifiants de notre nouvel utilisateur à la place de ceux de
notre utilisateur 'root'

##### Accès avec Apache2

    gem install --no-ri --no-rdoc passenger
    passenger-install-apache2-module -a

Il faut ensuite configurer apache correctement

    nano /etc/apache2/mods-available/passenger.load
    LoadModule passenger_module /var/lib/gems/1.9.1/gems/passenger-4.0.42/buildout/apache2/mod_passenger.so

    nano /etc/apache2/mods-available/passenger.conf
    <IfModule mod_passenger.c>
        PassengerRoot /var/lib/gems/1.9.1/gems/passenger-4.0.42
        Pass  engerDefaultRuby /usr/bin/ruby1.9.1
    </IfModule>

    nano /etc/apache2/sites-available/snorby
    <VirtualHost *:80>
        ServerName localhost
        DocumentRoot /var/www/snorby/public
        <Directory /var/www/snorby/public>
           # This relaxes Apache security settings.
           AllowOverride all
           # MultiViews must be turned off.
           Options -MultiViews
           # Uncomment this if you're on Apache >= 2.4:
           #Require all granted
        </Directory>
    </VirtualHost>

Nous devons ensuite activer différents modules pour que snorby fonctionne:

    a2enmod passenger
    a2enmod rewrite
    a2enmod ssl
    a2ensite snorby
    chown www-data:www-data -R /var/www/snorby
    service apache2 restart

Maintenant il suffit d'aller à l'adresse http://localhost/ pour vérifier le fonctionnement
de l'application. Si vous avez une erreur du type

    It looks like Bundler could not find a gem. Maybe you didn't install all the gems that this application needs. To install your gems, please run: bundle install

il suffit d'éxecuter les commandes suivantes:

    cd /var/www/snorby
    sudo -u www-data bundle pack
    sudo -u www-data bundle install --path vendor/cache

#### Installation de snort

Contrairement à Snorby, l'installation de Snort se fait très rapidement sous debian car il est dans les dépots.
Dans notre cas, nous allons installer snort-mysql pour pouvoir facilement nous interfacer avec snorby

    apt-get install snort-mysql

Après l'installation de snort, nous allons le configurer pour fonctionner avec la base de donnée de Snorby, pour celà:

    dpkg-reconfigure snort-mysql

Une fois dans l'assistant de configuration, il suffit d'entrer les information concernant l'interface qui va écouter sur le réseau
(par exemple eth0), puis veiller à activer le mode promiscuous permettant d'intercepter les paquets non destiné à notre machine.
Enfin accepter lorsque l'assistant demande si il faut configurer une base de donnée pour snort. Il suffit de mettre les informations
de connexion à la base de donnée Snorby et le tour est joué.

#### Troubleshoot

##### Snorby n'affiche pas les alertes

Si vous la page 'events' affichent bien les alertes mais qu'elles n'apparaissent pas dans la page d'accueil, il suffit
de vider le cache de la base de donnée et de relancer les workers pour cela:

    mysql -usnorby -p
    use snorby;
    truncate table caches;
    exit;

Le redémarrage des workers se fait dans l'interface web à partir du menu "Administration -> Worker & Job queue".

### Mettre en place du port knocking

#### Présentation

knockd est un daemon permettant de faire du port knocking sous Linux. Le service surveille toutes les connexions entrantes sur les interfaces de la machine,
si les connexions consécutives correspondent à une séquence de «knocks» qu'il a dans sa configuration, alors il execute une commande permettant d'ouvrir un port.

#### Installation

Pour installer knockd sous debian c'est très simple. Celui-ci est disponible dans les paquets de Debian Wheezy et peut donc être installé avec la commande suivante:

    apt-get install knockd

Après avoir installer knockd, il est neccessaire de modifier le fichier de configuration */etc/default/knockd* pour lui donner l'autorisation de démarrer.
Pour celà, il suffit de modifier la ligne suivante:

    # Avant
    START_KNOCKD=0
    # Après
    START_KNOCKD=1

#### Configuration de knockd

Nous allons maintenant configuer knockd pour ouvrir le port 22 (ssh) lorsque des knocks sont détectés sur les ports 7000, 8000 et 9000 en TCP.
Nous allons donc modifier le fichier **/etc/knockd.conf** et lui appliquer la configuration suivante.

    [opencloseSSH]
            sequence    = 7000,8000,9000
            seq_timeout = 5
            start_command     = /sbin/iptables -I INPUT 1 -s %IP% -p tcp --dport 22 -j ACCEPT
            tcpflags    = syn
            cmd_timeout   = 10
            stop_command  = /sbin/iptables -D INPUT -s %IP% -p tcp --dport 22 -j ACCEPT

Nous demandons donc à knockd d'ouvrir le port en rajoutant une règles iptables quand les knocks correspondent. Nous lui demandons aussi de refermer ce port au bout de 10 secondes.
Par défaut les ports indiqué doivent être «knocké» en TCP, mais il est aussi possible de demander de l'UDP. Pour ça il suffit d'indiquer le port suivant de son protocôle
de la manière suivante: *num_port:protcole*.

Nous pouvons maintenant démarrer knockd:

    service knockd start
    
#### Configuration iptables

Nous allons maintenant définir quelques règles iptables pour profiter de knockd. En effet, de base, debian ne définis aucune règle de filtrage. Tout les ports sont
donc ouvert par défaut. Nous allons donc définir des règles permettant de laisser passer certaines trames comme celle transitant sur notre boucle locale. Puis nous
demanderons à iptables de droper tout les paquets en dernier recours:

    iptables -A INPUT -i lo -j ACCEPT
    iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
    iptables -A INPUT -j DROP

#### Ouvrir le port à distance

Pour éxecuter une suite de knock rien de plus simple. Il y a même plusieurs solutions pour le faire.

##### Le client knock

Lorsque l'on installe le serveur de port knocking «knockd», un client est automatiquement installé avec. Il se nomme subtilement **knock**.
Pour l'utiliser rien de plus simple:

    knock ip_server 7000 8000 9000

Et par exemple si le port 8000 doit être «knocké» en UDP:

    knock ip_server 7000 8000:udp 9000
    
##### NMAP

Il est aussi d'utiliser le scanner de port NMAP pour effectuer des knocks:

    nmap -Pn --host_timeout 201 --max-retries 0  -p 7000 ip_server
    nmap -Pn --host_timeout 201 --max-retries 0  -p 8000 ip_server
    nmap -Pn --host_timeout 201 --max-retries 0  -p 9000 ip_server

### Se prémunir du DHCP spoofing
### Se protéger des attaques SMURF